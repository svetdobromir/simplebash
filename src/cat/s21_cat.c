
// Добавить -v ???
// Добавить непечатаемые символы ???
/// Добавить ошибки на неизвестный флаг
// Добавить ли проверку на ошибочный ввод?

#include <stdio.h>
#include <string.h>

typedef struct flags {
    char flag_end;
    char flag_compress;
    char flag_tab;  //  Возможно удалить
    char flag_start_str;
    char flag_v;
} struc;

void create_struct(char *flag, struc *Flags) {
    //  Нумерует только непустые строки
    if (strcmp(flag, "-b") == 0 || strcmp(flag, "--number-nonblank") == 0) {
        Flags->flag_start_str = 'b';
    }
    // Нумерует все строки
    if ((strcmp(flag, "-n") == 0 || strcmp(flag, "--number") == 0) && Flags->flag_start_str != 'b') {
        Flags->flag_start_str = 'n';
    }
    // Отображение символа конца строки
    if (strcmp(flag, "-e") == 0 || strcmp(flag, "-E") == 0) {
        Flags->flag_end = 'e';
    }
    if (strcmp(flag, "-E") == 0) {
        Flags->flag_end = 'E';
    }

    //  Сжать несколько пустых строк
    if ((strcmp(flag, "-s") == 0 || strcmp(flag, "--squeeze-blank") == 0)) {
        Flags->flag_compress = 's';
    }

    // Отображение табов
    if (strcmp(flag, "-t") == 0) {
        Flags->flag_tab = 't';
    }
    if (strcmp(flag, "-T") == 0) {
        Flags->flag_tab = 'T';
    }
    //  Символ конца строки + таб
    if (strcmp(flag, "-v") == 0) {
        Flags->flag_v = 'v';
    }
}
void show_struct(struc *Flags) {
    if (Flags->flag_tab == 't')
        printf("-t");
    if (Flags->flag_compress == 's')
        printf("-s");
    if (Flags->flag_end == 'e')
        printf("-e");
}

void print_symb(char ch, struc Flags) {
    if (Flags.flag_tab == 't' || Flags.flag_end == 'e' || Flags.flag_v == 'v') {
        unsigned char c = (unsigned char)ch;
        int left = (c <= 31 && c != 9 && c != 10);
        int right = (c >= 128 && c <= 159);
        int mostright = 0;  // (c > 160 && c <= 191);
        if (right) {
            printf("%s%c", "M-^", c - 64);
        } else if (left) {
            printf("%s%c", "^", c + 64);
        } else if (mostright) {
            printf("%s%c", "M-^", c - 128);
        } else if (c == 127) {
            printf("^?");
        } else {
            printf("%c", ch);
        }
    } else {
        printf("%c", ch);
    }
}

int main(int argc, char **argv) {
    FILE *file;
    struc Flags;
    Flags.flag_end = 'r';
    Flags.flag_compress = 'r';
    Flags.flag_tab = 'r';   //  Возможно удалить
    Flags.flag_start_str = 'r';
    Flags.flag_v = 'r';
    char c = '0';
    if (argc == 1) {  //  Active mode?()
        while (1) {
        c = getchar();
        if (c == EOF)
            break;
        putchar(c);
        }
     }

    if (argc > 1) {
        char * p;
        int i = 1;
        int last_flag_position = 0;
        while (1) {
            p = argv[i++];
            if (p == NULL) {
                break;
            } else if (p[0] == '-') {
                create_struct(p, &Flags);
                last_flag_position++;
            } else {
                last_flag_position++;
                break;
            }
        }
        unsigned long counter_str = 1;
        char ch;
        int flag_empty = 0, flag_compress = 0;
        int flag_file_error = 0;

        while (1) {
            if (argv[last_flag_position] != NULL) {
                file = fopen(argv[last_flag_position], "r");
            } else {
                break;
            }
            if (file == NULL && argv[last_flag_position] != NULL) {
                printf("cat: %s: %s\n", argv[last_flag_position++], strerror(2));
                continue;
            } else if  (file == NULL && argv[last_flag_position] == NULL) {
                break;
            }
            last_flag_position++;
            flag_file_error++;
            while ((ch=fgetc(file)) != EOF) {   //  Пока файл существует
                flag_empty = 1;   //  Флаг начала строки
                if (ch != '\n') {   //  Пока нет символа следующей строки.
                    flag_compress = 0;
                    while (1) {
                        if (ch == EOF)  // Если конец файла, выходим
                            break;
                        if ((Flags.flag_start_str == 'b' || Flags.flag_start_str == 'n') && flag_empty == 1) {
                            printf("%6lu\t", counter_str++);
                            flag_empty = 0;  //  Очищаем флаг начального символа
                        }
                        if (ch == '\t' && (Flags.flag_tab == 't' || Flags.flag_end == 'T')) {
                            printf("^I");
                            ch = fgetc(file);
                            continue;
                        }
                        if (ch == '\n' && (Flags.flag_end == 'e' || Flags.flag_end == 'E')) {
                            printf("$");
                            putchar('\n');
                            break;
                        }
                        print_symb(ch, Flags);
                        if (ch == '\n') {
                            break;
                        }
                        ch = fgetc(file);
                    }
                } else {
                    if (Flags.flag_compress == 's') {
                        if (flag_compress == 0) {
                            flag_compress = 1;
                        } else {
                            continue;
                        }
                    }
                    if (Flags.flag_start_str == 'n') {  /// !!! НЕ нумерует
                            printf("%6lu\t", counter_str++);
                            flag_empty = 0;
                    }
                    if ((Flags.flag_end == 'e' || Flags.flag_end == 'E')) {
                            printf("$");
                    }
                    print_symb(ch, Flags);
                }
            }
            fclose(file);
        }
    }
}
