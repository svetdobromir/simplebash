import itertools
import subprocess
st = b"when in the course of fooman events\n"
#s = subprocess.check_output(["cat", "-b", "text.txt"])
import pathlib
import os
import sys

string_flags = ""

def read_file():
    list_flags = list()
    with open("tests/flags.txt", "r") as file:
        for line in file:
            list_flags.append(line[:-1])
    return list_flags

def read_checking_file():
    with open("tests/bytes.txt", "r") as file:
        checking_file = file.read()
    return checking_file


def checking(list_flags):
    text_file = " tests/bytes.txt"
    for flags in list_flags:
        cmd_origin= "cat " + flags  + text_file
        s_origin = subprocess.check_output(cmd_origin,  shell=True)
        s_origin = s_origin.decode('UTF-8')
        cmd = "./s21_cat " + flags + text_file
        s_21cat = subprocess.check_output(cmd, shell=True)
        s_21cat = s_21cat.decode('UTF-8')
        
        if (s_origin == s_21cat):
            print(f"Flag {flags}:")
            print("Ok")
        else:
            print(flags)
            print("+++++++++++++ORIGIN++++++++++++")
            print(s_origin)
            print("+++++++++++++S21_CAT+++++++++++")
            print( s_21cat)
            break

def checking_val(list_flags):
    text_file = " tests/bytes.txt"
    for flags in list_flags:
        cmd = "valgrind --leak-check=full ./s21_cat " + flags + text_file + " | grep \"ERROR SUMMARY\" "
        try:
            s_21cat = subprocess.check_output(cmd, shell=True)
            s_21cat = s_21cat.decode('UTF-8')
            print(s_21cat)
        except subprocess.CalledProcessError as e:
            print (e.output)

list_flags = read_file()
#checking_file = read_checking_file()
try:
    if (sys.argv[1] == "val"):
        checking_val(list_flags)
except:
    checking(list_flags)
