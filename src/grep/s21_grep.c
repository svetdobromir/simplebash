///  Добавить ошибку на fopen
/// УБЕДИТЬСЯ ЧТО ВСЕ ФАЙЛЫ С ПАТТЕРНАМИ ЗАКРЫТЫ


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <regex.h>

typedef struct flag_s {
    char ignore_register_i;
    char invert_search_v;
    char number_of_string_c;
    char number_of_line_n;
    char string_without_names_h;
    char no_error_message_s;
    char print_only_coincidence_o;
    char flag_ef;
    char only_files_l;
} Flags;

unsigned long size_of_line(FILE *file) {
    int currsor_position_start = ftell(file);
    char c;
    unsigned long size = 0;
    while (1) {
        c = fgetc(file);
        if (c == '\n')
            break;
        if (c == EOF) {
            size++;
            break;
        }
    }
    size = size + ftell(file) - currsor_position_start;
    fseek(file, currsor_position_start, SEEK_SET);
    return size-1;
}

void int_to_string(char *str, long int num) {
  int x = 0, len = 0, start = 0;
  if (num < 0) {
    *str = '-';
    start = 1;
    len++;
  } else if (num == 0) {
    len++;
    *str = '0';
  }
  if (num < 0) {
    for (long int temp_num = num; temp_num <= -1; temp_num /= 10) {
      len++;
    }
  } else {
    for (long int temp_num = num; temp_num >= 1; temp_num /= 10) {
      len++;
    }
  }
  str[len] = '\0';
  for (int i = len - 1; i >= start; i--) {
    x = num < 0 ? -(num % 10) : num % 10;
    str[i] = x + '0';
    num /= 10;
  }
}

int add_flag_to_struct(Flags *struct_flags, char* flag) {
    int ret = 0;
    for (int i = 1; i < (int)strlen(flag); i++) {
        switch (flag[i]) {
        case 'i':
            struct_flags -> ignore_register_i = 'i';
            break;
        case 'v':
            struct_flags ->invert_search_v = 'v';
            break;
        case 'c':
            struct_flags -> number_of_string_c = 'c';
            break;
        case 'n':
            struct_flags -> number_of_line_n = 'n';
            break;
        case 'h':
            struct_flags -> string_without_names_h = 'h';
            break;
        case 's':
            struct_flags -> no_error_message_s = 's';
            break;
        case 'o':
            struct_flags -> print_only_coincidence_o = 'o';
            break;
        case 'e':
            struct_flags -> flag_ef = '1';
            break;
        case 'f':
            struct_flags -> flag_ef = '1';
            break;
        case 'l':
            struct_flags -> only_files_l = 'l';
            break;
        default:
            ret = 1;
            break;
        }
    }
    return ret;
}

void show_struct(Flags *struct_flags) {
    printf("I = %c\n", struct_flags -> ignore_register_i);
    printf("V = %c\n", struct_flags ->invert_search_v);
    printf("C = %c\n", struct_flags -> number_of_string_c);
    printf("N = %c\n", struct_flags -> number_of_line_n);
    printf("H = %c\n", struct_flags -> string_without_names_h);
    printf("S = %c\n", struct_flags -> no_error_message_s);
    printf("O = %c\n", struct_flags -> print_only_coincidence_o);
    printf("EF = %c\n", struct_flags -> flag_ef);
    printf("l = %c\n", struct_flags -> only_files_l);
}

void null_struct(Flags *struct_flags) {
    struct_flags -> ignore_register_i = '0';
    struct_flags ->invert_search_v = '0';
    struct_flags -> number_of_string_c = '0';
    struct_flags -> number_of_line_n = '0';
    struct_flags -> string_without_names_h = '0';
    struct_flags -> no_error_message_s = '0';
    struct_flags -> print_only_coincidence_o = '0';
    struct_flags -> flag_ef = '0';
    struct_flags -> only_files_l = '0';
}

void free_arrays(char**array_patterns, char **array_file) {
    int i = 0;
    while (array_patterns[i] != NULL) {
        free(array_patterns[i]);
        i++;
    }
    i = 0;
    while (array_file[i] != NULL) {
        free(array_file[i]);
        i++;
    }
}

void test_show_array(char**array_patterns) {
    int i = 0;
    while (array_patterns[i] != NULL) {
         printf("Patterns %d = %s\n", i, array_patterns[i]);
        i++;
    }
}

void test_show_array_file(char ** array_files) {
    int i = 0;
    while (array_files[i] != NULL) {
        printf("File %d = %s\n", i, array_files[i]);
        i++;
    }
}

int find_pattern(int argc, char ** argv, char * pattern, char ** array_pattern, Flags *struct_flags) {
    char pattern_flag[] = "^-";  //  pattern для поиска флагов

    regex_t preg;  // структура с шаблоном
    regcomp(&preg, pattern_flag, REG_EXTENDED);  // Добавляем структуру в шаблон
    regmatch_t pm;
    int error_code = 0;
    int flag_is_pattern = 0;  // Говорит, что есть шаблон.
    FILE *pattern_file;
    int count_pattern = 0;
    for (int i = 1; i < argc; i++) {  // Начинаем с единицы, так как нулевой аргумент это название файла.
        // Прогоняем цикл еще раз. Если находим e или f - запускаем получение шаблона
        if ((!strcmp(argv[i], "-e")) || !(strcmp(argv[i], "-f"))) {  // Находим флаг, парсим шаблон
            if (i == argc-1) {
                error_code = -1;
                break;
            }
            count_pattern++;
            if (argv[i+1] != NULL) {
                if ((regexec(&preg, argv[i], 0, &pm, 0) != 0)) {
                    error_code = -1;  // Значит что после флага -e или -f идет еще флаг  -e или -f
                    break;
                }
                //  Так как следующим аргументов должен быть либо шаблон либо файл.
                if (strchr(argv[i], 'e')) {
                    strcpy(pattern, argv[i+1]);
                    array_pattern[flag_is_pattern] = malloc( sizeof(char) * (strlen(argv[i+1]) + 1));
                    if (array_pattern[flag_is_pattern] == NULL) {
                        exit(1);  // ВЫХОД ЕСЛИ ВДРУГ ОШИБКА МАллока
                    }
                    strcpy((array_pattern)[flag_is_pattern], argv[i+1]);
                    flag_is_pattern++;
                } else if (strchr(argv[i], 'f')) {  ////  Парсинг из файла
                    pattern_file = fopen(argv[i+1], "r");
                    if (pattern_file == NULL) {
                        error_code = 2;
                        if (struct_flags->no_error_message_s != 's') {
                            printf("grep: %s: %s\n", argv[i+1], strerror(2));
                        }
                        break;
                    }
                    while (fgets(pattern, 1024, pattern_file) != NULL) {
                        if (pattern[0] != '\n') {
                            if (pattern[strlen(pattern)-1] == '\n') {
                                pattern[strlen(pattern)- 1] = '\0';
                            }
                            array_pattern[flag_is_pattern] = malloc( sizeof(char) * (strlen(pattern) + 1));
                            if (array_pattern[flag_is_pattern] == NULL) {
                                exit(1);  // ВЫХОД ЕСЛИ ВДРУГ ОШИБКА МАллока
                            }
                            strcpy(array_pattern[flag_is_pattern], pattern);
                            flag_is_pattern++;
                        }
                    }
                    fclose(pattern_file);
            }
            } else {
                if (struct_flags->no_error_message_s != 's') {
                    printf("File pattern not found\n");
                }
                break;
            }
        } else if ((regexec(&preg, argv[i], 0, &pm, 0) != 0) && flag_is_pattern == 0 &&
                    struct_flags->flag_ef != '1' && i != (argc-1)) {
                array_pattern[flag_is_pattern] = malloc( sizeof(char) * (strlen(argv[i+1]) + 1024));
                if (array_pattern[flag_is_pattern] == NULL) {
                    exit(1);  // ВЫХОД ЕСЛИ ВДРУГ ОШИБКА МАллока2
                }
                strcpy(array_pattern[flag_is_pattern], argv[i]);
                flag_is_pattern++;
        }
    }
    if (flag_is_pattern == 0) {  // Паттерн не найден. Выход из программы
         error_code = -1;
    }
        regfree(&preg);
    return error_code;
}

int find_files(int argc, char ** argv, char ** array_file, Flags * struct_flags) {
    char pattern_flag[] = "^-";
    regex_t preg;  // структура с шаблоном
    regcomp(&preg, pattern_flag, REG_EXTENDED);  // Добавляем структуру в шаблон
    regmatch_t pm;
    int code_error = 0;
    int count_array = 0;
    int k = 2;
    if (struct_flags->flag_ef == '1') {
        k = 1;
    }

    for (int i = k; i < argc; i++) {
        if ( regexec(&preg, argv[i], 0, &pm, 0) != 0  &&
                strcmp(argv[i-1], "-e") && strcmp(argv[i-1], "-f") &&
                !(regexec(&preg, argv[i-1], 0, &pm, 0) == 0 && i == 2)) {
            array_file[count_array] = malloc(sizeof(char) * (strlen(argv[i]) + 1));
            if (array_file[count_array] == NULL) {
                exit(1);  // ВЫХОД ЕСЛИ ВДРУГ ОШИБКА МАллока
            }
            strcpy(array_file[count_array], argv[i]);
            count_array++;
        }
    }
    regfree(&preg);
    return code_error;
}

int what_regex_param(Flags struct_flags) {
    int param = 0;
    if (struct_flags.ignore_register_i == 'i') {
        param = 2;
    } else {
        param = 1;
    }
    return param;
}

int how_many_files(char ** array_file) {
    int count = 0;
    while (array_file[count] != NULL) count++;
    return count;
}

//  Принтер формата, учитывая флаг h
void print_format(char * file_name, char count_files,
        unsigned long num_string, Flags struct_flags) {
    if (count_files > 1 && struct_flags.string_without_names_h != 'h') {
        if (struct_flags.number_of_line_n == 'n' && num_string != 0) {
            printf("%s:%lu:", file_name, num_string);
        } else {
            printf("%s:", file_name);
        }
    }
    if (count_files == 1 || struct_flags.string_without_names_h == 'h') {
         if (struct_flags.number_of_line_n == 'n' && num_string != 0) {
            printf("%lu:", num_string);
        }
    }
}

void printer(regex_t preg, char* line, char * file_name, char count_files,
        unsigned long num_string, Flags struct_flags) {
    char* line_file = line;
    regmatch_t pm;
    int regerr;
    while ((regerr = regexec(&preg, line_file, 1, &pm, 0)) == 0) {
        print_format(file_name, count_files,
        num_string, struct_flags);
        for (char* curr = (line_file + pm.rm_so);
                curr < line_file + pm.rm_eo; curr++) {
            putchar(*curr);
        }
        line_file = line_file + pm.rm_eo;
        printf("\n");
    }
}

char* right_pointer_in_string(char *pattern, char* line, int regex_param) {
    regex_t preg;
    regcomp(&preg, pattern, regex_param);
    char* line_file = &line[0];
    regmatch_t pm[0];
    pm[0].rm_eo = 0;
    pm[0].rm_so = 0;
    int regerr = 0;
    char * right_pointer = line_file;
    if (line_file != NULL) {
        while ((regerr = regexec(&preg, line_file, 1, pm, REG_NEWLINE)) != 1) {
            if (line_file+pm->rm_so >= right_pointer) {
                right_pointer = line_file + pm->rm_so;
            }
            line_file = line_file + pm->rm_eo;
            if (line_file == NULL) {
                break;
            }
        }
    }
    regfree(&preg);
    if (right_pointer == NULL) {
        right_pointer = 0;
    }
    return right_pointer;
}

//  Возврашаем найденный паттерн или NULL
char* is_pattern_in_string(char*line_file, char** array_patterns,
        int regex_param, Flags struct_flags) {
    int i_pattern = 0;
    regex_t preg;
    int err_regcomp = 0, result_regcomp = 0;
    regmatch_t pm;
    char*  finded_pattern = NULL;
    while (array_patterns[i_pattern] != NULL) {
        err_regcomp = regcomp(&preg, array_patterns[i_pattern],
            regex_param);  // Записываем паттерн
        if (err_regcomp != 0) {
            if (struct_flags.no_error_message_s != 's') {
                printf("grep: Invalid pattern\n");
                regfree(&preg);
                i_pattern++;
                continue;
            }
        }
        result_regcomp = regexec(&preg, line_file, 1, &pm, 0);
        if (result_regcomp == 0) {
            finded_pattern = array_patterns[i_pattern];
        }
        regfree(&preg);
        i_pattern++;
    }
    return finded_pattern;
}

int searching_pattern_in_text(char ** array_file,
        char ** array_patterns, Flags struct_flags) {
    FILE * file;
    regex_t preg;  //  структура с шаблоном
    int regex_param = what_regex_param(struct_flags);
    char str_to_counter_string[25] = {0};
    unsigned long  counter_string = 0;
    unsigned long  absolute_counter_string = 0;
    int count_files = how_many_files(array_file);  // Количество файлов
    int i_file = 0;
    int end_of_file_flag = 0;
    char c = '0';  // Символ файла
    unsigned long counter_char = 1;  //  Счетчик символа в файле
    int result_regcomp = 0;
    char *line_file = NULL;  // Указатель на строку
    while (array_file[i_file] != NULL) {
        if (array_file[i_file] != NULL) {
            file = fopen(array_file[i_file], "r");  // Открываем файл
        } else {
            break;  //  Выход, если конец патерна
        }

        if (file == NULL) {  // Обработка ошибки отсутствия файла
            if (struct_flags.no_error_message_s != 's') {
                printf("grep: %s: %s", array_file[i_file], strerror(2));
            }
            i_file++;
            continue;
        }
        end_of_file_flag = 0;  // Символ флаг конца файла
        absolute_counter_string = 0;  // Обнуляем счетчик строк
        while (1) {
            line_file = NULL;
            //  Начинаем листать файл.
            line_file = malloc(sizeof(char) * counter_char + 1024);
            memset(line_file, '\0', 1024);
            while ((c=getc(file)) != '\n') {
                if (c == EOF) {
                    counter_char++;
                    line_file[counter_char - 2] =  '\0';
                    end_of_file_flag = 1;
                    break;
                }
                counter_char++;
                line_file = realloc(line_file, (sizeof(char) * counter_char + 1024));
                line_file[counter_char-2] =  c;
            }
            if (c == EOF && counter_char == 2) {  // Если конец строки
                counter_char = 1;
                free(line_file);
                line_file = NULL;
                break;  // не обнулили
            }
            absolute_counter_string++;
            line_file[counter_char-1] = '\0';
            // Здесь мы находим КОНЕЧНЫЙ паттерн в строке, если он существует
            char * pattern = NULL;
            if (strchr(line_file, '\0') != NULL) {
                pattern = is_pattern_in_string(line_file,
                            array_patterns, regex_param, struct_flags);
            }
            if (pattern != NULL) {
                result_regcomp = 0;
            } else {
                result_regcomp = 1;
            }
                // УМНЫЙ СЧЕТЧИК
                if (result_regcomp == 0 &&
                    struct_flags.invert_search_v != 'v') {
                        counter_string++;
                } else if (result_regcomp == 1 &&
                        struct_flags.invert_search_v == 'v') {
                        counter_string++;  // Инвентируемый случай
                }

                if (pattern != NULL) {
                    regcomp(&preg, pattern, regex_param);
                }

                ///  ПРОСТО ПРИНТ // Печатует если нет повторения в строке
                if (struct_flags.number_of_string_c != 'c' &&
                    struct_flags.only_files_l != 'l') {
                    if (result_regcomp == 0 &&
                        struct_flags.invert_search_v != 'v') {
                        if (struct_flags.print_only_coincidence_o == 'o') {
                            printer(preg, line_file, array_file[i_file], count_files,
                                    absolute_counter_string, struct_flags);
                        } else {
                            print_format(array_file[i_file], count_files,
                                    absolute_counter_string, struct_flags);
                            printf("%s\n", line_file);
                        }
                    } else if (result_regcomp == 1 &&
                            struct_flags.invert_search_v == 'v') {
                        print_format(array_file[i_file], count_files,
                                absolute_counter_string, struct_flags);
                        printf("%s\n", line_file);
                    }
                }
            if (pattern != NULL) {
                regfree(&preg);
            }
            counter_char = 1;
            free(line_file);
            line_file = NULL;
            if (end_of_file_flag == 1) {
                break;   ///   Если конец файла, выходим из цикла.
            }
        }  //  Конец цикла для файла
        if (struct_flags.only_files_l == 'l') {
            if (counter_string > 0) {
                counter_string = 1;
            }
        }
        if (struct_flags.number_of_string_c == 'c') {
            int_to_string(str_to_counter_string, counter_string);
            print_format(array_file[i_file], count_files, 0, struct_flags);
            printf("%s\n", str_to_counter_string);
        }
        if (struct_flags.only_files_l == 'l' && counter_string > 0) {
            printf("%s\n", array_file[i_file]);
        }
        fclose(file);   //  Закрываем файл перед открытием нового
        counter_string = 0;   //  Обнуляем счетчик совпадающих строк
        i_file++;
    }
    return 0;
}

int main(int argc, char **argv) {
    char pattern[1000] = {0};
    char * array_patterns[1024] = {0};
    char * array_file[1024] = {0};
    int i = 1;
    char * flag = NULL;
    int error_code = 0;
    Flags struct_flags;
    null_struct(&struct_flags);
    while ((flag = argv[i++]) != NULL) {   // Здесь добавляем флаги и считаем
        if (flag[0] == '-') {
            error_code = add_flag_to_struct(&struct_flags, flag);
            if (error_code == 1) {
                if (struct_flags.no_error_message_s == 's') {
                    printf("grep: invalid option\n");
                }
            }
        }
    }
    if (error_code == 0) {
        error_code = find_pattern(argc, argv, pattern,
                    array_patterns, &struct_flags);
    }
    if (error_code == 0) {
        error_code = find_files(argc, argv, array_file, &struct_flags);
        if (error_code == 0) {
            searching_pattern_in_text(array_file, array_patterns, struct_flags);
        }
    }
    //  test_show_array_file(array_file);
    //  test_show_array(array_patterns);
    free_arrays(array_patterns, array_file);
    return 0;
}
